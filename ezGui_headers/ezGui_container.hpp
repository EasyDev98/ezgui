/*
 * ezGui_container.hpp
 * 
 * Copyright 2017 easy-dev <easy-dev@web.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
 
 /* 
  * Class description:
  * 
  * Combines all you're used elements(if you want) into one std::vector
  * object. This object can later be processed by ezGui_Event class
  * if intended. Only used to make things "easier". You don't need to
  * use this class and you can disable the include in ezGui.h. If you do
  * so, you will be unable to use the ezGui_Event class, due to it's then
  * "useless" because it "relias" on this class. It is possible to 
  * interrupt this reliability by editing the ezGui_Event class.
  * 
  */ 
 
class ezGui_Container {
	private:
		std::vector<ezGui_Widget> widgetContainer;
		
	public:
		void add(ezGui_Widget addWidget);
		void del();
		void clear();
		void erase(int widgetID);
		
		void setContainer(std::vector<ezGui_Widget> container);
		std::vector<ezGui_Widget> getContainer();
};

void ezGui_Container::add(ezGui_Widget addWidget) {
	widgetContainer.push_back(addWidget);
}

void ezGui_Container::del() {
	widgetContainer.pop_back();
}

void ezGui_Container::clear() {
	widgetContainer.clear();
}

//Maybe out their exists a faster way...!
void ezGui_Container::erase(int widgetID) {
	std::vector<ezGui_Widget> tempContainer;
	for(int i=0;i<=widgetContainer.size();i++) {
		if(i!=widgetID) {
			tempContainer.push_back(widgetContainer.at(i));
		}
	}
	widgetContainer=tempContainer;
}

void ezGui_Container::setContainer(std::vector<ezGui_Widget> container) { widgetContainer=container; }
std::vector<ezGui_Widget> ezGui_Container::getContainer() { return widgetContainer; }
