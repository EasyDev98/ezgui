/*
 * ezGui_widget.hpp
 * 
 * Copyright 2017 easy-dev <easy-dev@web.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
class ezGui_Widget {
	private:
		int widgetX;
		int widgetY;
		int widgetWidth;
		int widgetHeight;
		static int widgetIDCounter;
		int widgetID;
		int widgetEvent;
		SDL_Renderer* widgetRenderer;
		
	public:
		ezGui_Widget();
		void create(SDL_Renderer* renderer,int x,int y,int w,int h);
		void close();
		
		void setWidgetX(int x);
		void setWidgetY(int y);
		void setWidgetW(int w);
		void setWidgetH(int h);
		void setWidgetEvent(int event);
		void setWidgetRenderer(SDL_Renderer* renderer);
		
		int getWidgetX();
		int getWidgetY();
		int getWidgetW();
		int getWidgetH();
		int getWidgetEvent();
		int getWidgetID();
		SDL_Renderer* getRenderer();
};

int ezGui_Widget::widgetIDCounter=1;

ezGui_Widget::ezGui_Widget() {
	widgetID=widgetIDCounter;
	widgetIDCounter++;
}

void ezGui_Widget::create(SDL_Renderer* renderer,int x,int y,int w,int h) {
	widgetX=x;
	widgetY=y;
	widgetWidth=w;
	widgetHeight=h;
	widgetRenderer=renderer;
}

void ezGui_Widget::close() {
	SDL_DestroyRenderer(widgetRenderer);
}

void ezGui_Widget::setWidgetX(int x) { widgetX=x; }
void ezGui_Widget::setWidgetY(int y) { widgetY=y; }
void ezGui_Widget::setWidgetW(int w) { widgetWidth=w; }
void ezGui_Widget::setWidgetH(int h) { widgetHeight=h; }
void ezGui_Widget::setWidgetEvent(int event) { widgetEvent=event; }
void ezGui_Widget::setWidgetRenderer(SDL_Renderer* renderer) { widgetRenderer=renderer; }

int ezGui_Widget::getWidgetX() { return widgetX; }
int ezGui_Widget::getWidgetY() { return widgetY; }
int ezGui_Widget::getWidgetW() { return widgetWidth; }
int ezGui_Widget::getWidgetH() { return widgetHeight; }
int ezGui_Widget::getWidgetEvent() { return widgetEvent; }
int ezGui_Widget::getWidgetID() { return widgetID; }
SDL_Renderer* ezGui_Widget::getRenderer() { return widgetRenderer; }
