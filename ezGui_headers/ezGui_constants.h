/*
 * ezGui_constants.h
 * 
 * Copyright 2017 easy-dev <easy-dev@web.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/*
 * Gui constants:
 * 
 * These are some often used constants in SDL2. In my opinion their are
 * sometimes too long, so I decided to create some short versions for 
 * you! If you want you can banish this file from being included in the
 * ezGui.h header file or just comment the block of constants below...
 * 
 */
 
const int WIN_DEF_POS=SDL_WINDOWPOS_UNDEFINED;
const int WIN_CEN_POS=SDL_WINDOWPOS_CENTERED;

const Uint32 WIN_BORDERLESS=SDL_WINDOW_BORDERLESS;	
const Uint32 WIN_RESIZABLE=SDL_WINDOW_RESIZABLE;	
const Uint32 WIN_OPENGL=SDL_WINDOW_OPENGL;
const Uint32 WIN_HIDDEN=SDL_WINDOW_HIDDEN;
const Uint32 WIN_HIGHDPI=SDL_WINDOW_ALLOW_HIGHDPI;
const Uint32 WIN_MAX=SDL_WINDOW_MINIMIZED;
const Uint32 WIN_MIN=SDL_WINDOW_MAXIMIZED;
const Uint32 WIN_SHOW=SDL_WINDOW_SHOWN;
