/*
 * ezGui_mainClass.hpp
 * 
 * Copyright 2017 easy-dev <easy-dev@web.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
class ezGui_Main {
	private:
		SDL_Window* ezGui_SdlWindow;
		SDL_Renderer* ezGui_SdlRenderer;
		
	public:
		int init();
		int create(std::string guiTitle,int xpos,int ypos,int width,int height,Uint32 guiMode);
		int refresh();
		void present();
		void close();
		void fillBG(SDL_Color bgColor);
		
		SDL_Window* getWindow();
		SDL_Renderer* getRenderer();
		void setWindow(SDL_Window* sdlWindow);
		void setRenderer(SDL_Renderer* sdlRenderer);
};

int ezGui_Main::init() {
	if(SDL_Init(SDL_INIT_VIDEO)) {
		if(IMG_Init(IMG_INIT_PNG)) {
			if(TTF_Init()) {
				return 1;
			}
		}
	}
	return 0;
}

int ezGui_Main::create(std::string guiTitle,int xpos,int ypos,int width,int height,Uint32 guiMode) {
	ezGui_SdlWindow=SDL_CreateWindow(guiTitle.c_str(),xpos,ypos,width,height,guiMode);
	if(ezGui_SdlWindow) {
		ezGui_SdlRenderer=SDL_CreateRenderer(ezGui_SdlWindow,-1,SDL_RENDERER_ACCELERATED);
		if(ezGui_SdlRenderer) {
			return 1;
		}
	}
	return 0;
}

int ezGui_Main::refresh() {
	if(SDL_RenderClear(ezGui_SdlRenderer)) {
		return 1;
	}
	return 0;
}

void ezGui_Main::present() {
	SDL_RenderPresent(ezGui_SdlRenderer);
}

void ezGui_Main::close() {
	SDL_DestroyRenderer(ezGui_SdlRenderer);
	SDL_DestroyWindow(ezGui_SdlWindow);
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

SDL_Window* ezGui_Main::getWindow() {
	return ezGui_SdlWindow;
}

SDL_Renderer* ezGui_Main::getRenderer() {
	return ezGui_SdlRenderer;
}

void ezGui_Main::setWindow(SDL_Window* sdlWindow) {
	ezGui_SdlWindow=sdlWindow;
}

void ezGui_Main::setRenderer(SDL_Renderer* sdlRenderer) {
	ezGui_SdlRenderer=sdlRenderer;
}

void ezGui_Main::fillBG(SDL_Color bgColor) {
	SDL_SetRenderDrawColor(ezGui_SdlRenderer,bgColor.r,bgColor.g,bgColor.b,bgColor.a);
	SDL_RenderFillRect(ezGui_SdlRenderer,0);
}
