#include "ezGui.h"

int main(int argc, char* argv[]) {
	ezGui_Main gui;
	gui.init();
	gui.create("Test",WIN_CEN_POS,WIN_CEN_POS,500,500,WIN_SHOW);
	
	SDL_Event e;
	while(1) {
		SDL_PollEvent(&e);
		if(e.type==SDL_QUIT) {
			break;
		}
		gui.refresh();
		gui.fillBG({185,185,185,255});
		gui.present();
	}
	gui.close();
	return 0;
}
