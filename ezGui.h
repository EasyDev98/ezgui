/*
 * ezGui.h
 * 
 * Copyright 2017 easy-dev <easy-dev@web.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
#ifndef ezGUI_I
	#define ezGUI_I
	
	#include <string>
	#include <vector>
	#include <array>
	
	#ifdef _WIN32
		#include <SDL.h>
		#include <SDL_image.h>
		#include <SDL_ttf.h>
	#else
		#include <SDL2/SDL.h>
		#include <SDL2/SDL_image.h>
		#include <SDL2/SDL_ttf.h>
	#endif
	
	#include "ezGui_headers/ezGui_constants.h"
	#include "ezGui_headers/ezGui_mainClass.hpp"
	#include "ezGui_headers/ezGui_widget.hpp"
	#include "ezGui_headers/ezGui_container.hpp"
#endif
